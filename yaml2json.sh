#!/bin/sh

set -eu

# check if js-yaml is available
command -v js-yaml >/dev/null 2>&1 || {
  echo >&2 "Please install 'js-yaml' using: npm i -g js-yaml"
  exit 1
}

# configuration
if [ "$#" -ge 1 ]; then
  start_dir="$1"
else
  start_dir="."
fi

# $1: directory where to search files
generate_json() {
  find "$1" -type f -maxdepth 1 -not -path '*/\.*' -name *.yaml | sort | \
  while IFS='' read -r file_path; do
    json_path=$(echo "$file_path" | sed 's/\.yaml$/.json/')
    echo "Generate: $file_path → $json_path"
    js-yaml -t -c "$file_path" > "$json_path"
  done
}

echo "Generating json files from yaml…"

find "$start_dir" -type d -not -path '*/\.*' | \
while IFS='' read -r directory_path; do
  generate_json "$directory_path"
done
